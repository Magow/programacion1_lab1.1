#!/usr/bin/env python3
#! -*- coding:utf-8 -*-

#promedio de numeros
def promedio(lista1):

    for i in range(len(lista1)):
        print("valores de la lista1: ", lista1[i])

    promedio = sum(lista1)*len(lista1)
    return promedio


#cuadrados de numeros en lista
def cuadrados(lista2):

    listaresultante = []

    print("cuadrados de numeros de la siguiente lista: ")
    for i in range(len(lista2)):
        print(lista2[i])

    print("los cuadrados de cada valor en la lista son: ")
    for i in range(len(lista2)):
        listaresultante.append(lista2[i]**2)

    return listaresultante

#contador letras
def cuenta_letras(lista3):

    print("contador de letras: ")
    for i in range(len(lista3)):
        print(lista3[i])
        palabra_larga = max(lista3)

    return palabra_larga

#declaración de listas
lista1 = [1.0, 4.0, 52.0, 33.0, 77.0, 5.0]
lista2 = [2.0, 3.0, 4.0, 10.0, 6.0]
lista3 = ['hola', 'chao', 'programación', 'eeeeeeeo', 'bonjour']

#inicio de programa

print("el promedio de la lista1: ", promedio(lista1))
print( cuadrados(lista2))
print("la palabra mas larga de la lista es:", cuenta_letras(lista3))
